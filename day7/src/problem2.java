import java.util.Scanner;

public class problem2 {
    static int FibonacciX(int n){
        if(n == 1 ){
            return 0;
        } else if (n==2 ) {
            return 1;
        } else {
            return (FibonacciX(n-1) + FibonacciX(n-2));
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan nomor: ");
        int input = scan.nextInt();
        System.out.println("Nomor Fibonnaci ke-"+input+" = "+FibonacciX(input));

    }
}
