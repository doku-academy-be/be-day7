import java.util.*;

public class problem5 {
    public static void main(String[] args) {
       String[] elements ={"js", "js", "golang", "ruby", "ruby", "js", "js"};
        for (var val:elements
             ) {
            System.out.print(val+" ");

        }
        Map<String,Integer> nameAndCount = new HashMap<>();
        for (String name:elements
             ) {
            Integer count  = nameAndCount.get(name);
            if (count==null){
                nameAndCount.put(name,1);
            }
            else {
                nameAndCount.put(name, ++count);
            }
        }
        System.out.println(nameAndCount);
    }
}

